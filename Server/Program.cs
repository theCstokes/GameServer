﻿using GameCom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        public static int Main(String[] args)
        {
            AsyncServer server = new AsyncServer();
            server.Start();

            server.Stop();

            return 0;
        }
    }
}
