﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GameCom
{
    public abstract class BaseAsyncCom
    {
        public delegate void ReciveListener(string data);

        private List<ReciveListener> listeners;

        //protected Socket handler;
        //protected IPHostEntry ipHostInfo;
        //protected IPAddress ipAddress;
        protected IPEndPoint endPoint;

        public BaseAsyncCom(string ipAddress)
        {
            //ipHostInfo = Dns.Resolve(Dns.GetHostName());
            //ipAddress = ipHostInfo.AddressList[0];
            endPoint = new IPEndPoint(IPAddress.Parse(ipAddress), 11000);
            listeners = new List<ReciveListener>();
        }

        abstract public void Start();

        abstract public void Send(String data);

        abstract public void Stop();

        protected void Notify(string data)
        {
            listeners.ForEach((l) => l.Invoke(data));
        }

        protected void Send(Socket handler, String data)
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data + "<EOF>");

            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;

                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                content = state.sb.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    Notify(content);
                }
                else
                {
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        public static BaseAsyncCom operator+(BaseAsyncCom target, ReciveListener listener)
        {
            target.listeners.Add(listener);
            return target;
        }
    }
}
