﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GameCom
{
    class ComSocket
    {
        #region Public Members
        public delegate void ResponseListener(string s);
        #endregion

        #region Private Members
        private Socket s;
        private IPHostEntry ipHostInfo;
        private IPAddress ipAddress;
        private IPEndPoint remoteEP;
        private bool isRunning;
        private List<ResponseListener> listeners;
        #endregion

        public ComSocket()
        {
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);
            listeners = new List<ResponseListener>();
            isRunning = false;
        }

        #region Public Methods
        /// <summary> Starts the socket </summary>
        public void Start()
        {
            s = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            s.Connect(remoteEP);
            isRunning = true;
            AsyncListen();
        }

        ///// <summary> Stops the socket </summary>
        public void Stop()
        {
            isRunning = false;
            s.Disconnect(false);
        }

        public void send(byte[] data)
        {
            s.Send(data);
        }
        #endregion

        #region Private Mehtods
        /// <summary>
        /// Listen for a resoponse asyncronusly
        /// </summary>
        /// <returns></returns>
        private async Task AsyncListen()
        {
            await Listen();
        }

        private Task Listen()
        {
            Task t = new Task(() =>
            {
                Console.WriteLine("Started");
                while(isRunning)
                {
                    string data = ReciveNext();
                    if (data == null || !isRunning)
                    {
                        break;
                    }
                    listeners.ForEach((l) =>
                    {
                        l.Invoke(data);
                    });
                }
            });
            t.Start();
            return t;
        }

        private string ReciveNext()
        {
            string data = String.Empty;
            byte[] bytes;
            while(true)
            {
                if(!isRunning)
                {
                    return null;
                }
                bytes = new byte[1024];
                int bytesRecived = s.Receive(bytes);
                data += Encoding.ASCII.GetString(bytes, 0, bytesRecived);
                if(data.IndexOf("<EOF>") > -1)
                {
                    break;
                }
            }
            return data;
        }

        #endregion


        #region Operators
        /// <summary>
        /// Add item to listen for a response to this socket
        /// </summary>
        /// <param name="s"></param>
        /// <param name="listener"></param>
        /// <returns></returns>
        public static ComSocket operator +(ComSocket s, ResponseListener listener)
        {
            s.listeners.Add(listener);
            return s;
        }
        #endregion

    }
}
