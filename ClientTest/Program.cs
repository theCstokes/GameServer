﻿using GameCom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ClientTest
{
    class Program
    {
        public static int Main(String[] args)
        {
            AsyncClient client = new AsyncClient();
            client.Start();
            client.Send("This is a test");
            client.Stop();
            return 0;
        }
    }
}
